from django.apps import AppConfig


class PodcastConfig(AppConfig):
    name = 'wagtail_podcast'
