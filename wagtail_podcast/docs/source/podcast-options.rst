.. _podcast-options-label:

Podcast Options
===================

#.  Episode Type:
        This is what kind of episode you are publishing. It should usually be `Full`. Options are:
            #. Full - Normal full length episode. Appears as expected in apps and on website.
            #. Trailer - Teaser episode typically of a short length. Apps don't usually show them normally. Appears in normal list of podcasts on website.
            #. Bonus - Special releases outside of your normal cycle and will be marked as such by most apps. Appears in normal list of podcasts on website


#.  Author:
        The author of the podcast. List the institution if unsure

#.  Title:
        Title of the podcast

#.  Subtitle:
        Subtitle of the podcast

#.  Description:
        Short description of the podcast. Used to attract users on the app.

#.  Recording:
        The audio file you want to publish. It needs to be an MP3. Other formats will raise exceptions

#.  Explicit:
        Whether the podcast episode contains explicit content or not. Make sure you check the iTunes guidelines to help decide if you need to tick this. **If this is unticked and your content contains explicit content then your account may be suspended on Itunes/Google/Whatever.**

#.  Page Text:
        Text that accompanies your podcast. Shown on website and in app. Apps do not always display this.

#.  Image:
        Image that you want to use to represent your particular podcast episode. Use a high quality image as `wagtail_podcast` will deal with the resizing automatically.