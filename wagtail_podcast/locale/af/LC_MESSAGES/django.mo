��    ,      |  ;   �      �     �     �     �     �               !     *  $   J  	   o     y     �      �     �     �  "   �               %     *     B     H     [     d  ,   x     �     �     �  
   �     �  	                  -  	   ?  	   I     S     \     {     �     �  5   �  -   �  k       w     ~  $   �  !   �     �     �  	   �     �  $   	     :	     C	     b	     �	     �	     �	     �	     �	     �	     �	      
     
     
     0
     5
  &   J
     q
     �
     �
     �
     �
     �
     �
     �
          '  	   .     8     @     \     t     }  0   �  +   �               *       $   '                                 "                !      	            &            
   #                     %         )             (   +                ,                           Archive Author Author of particular podcast Author of podcast Blubrry Podcast Url Bonus Category Category of the podcast address Content that accompanies the podcast Copyright Copyright holder of podcast Cover image for podcast Description of nature of podcast Description of podcast Direct Email address of the podcast owner Episode Type Explicit Full Google Play Podcast Url Image Itunes Podcast Url Language Language of podcast Main text page that appears on the root page No description available No text available Owner email Owner name Owner of the podcast channel Page Text Podcast Podcast Directory URLs Podcast Root Page Recording Subscribe Subtitle Subtitle of particular podcast Subtitle of podacst Trailer Type of episode Whether the podcast channel contains explicit content Whether the podcast contains explicit content Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
                                          Argief Skrywer/Aanbieder Skywer/Aanbieder van hierdie podcast Skrywer/Aanbieder van die podcast Blubrry Podcast Url Bonus Kategorie Kategorie vir die podcast Teks inhoud wat met die podcast gaan Kopiereg Kopiereg houer van die podcast Kopireg houer van die podcast Beskrywing van die podcast Beskrywing van die podcast Direk Epos adres van podcast eienaar Episode Tiepe Eksplisiete Status Vol Google Play Podcast Url Foto Itunes Podcast Url Taal Taal van die podcast Teks wat op die indeks bladsy verskuin Geen beskywing beskikbaar nie Geen teks beskikbaar nie Eienaar se epos Eienaar se naam Eienaar van die podcast kanaal Bladsy Teks Podcast Podcast Directory URLs Podcast Vader Bladsy Opname Subscribe Byskrif Byskrif vir hierdie podcast Byskrif van die podcast Voorskou Tiepe episode Of daar eksplisiete inhoud op die kanaal voorkom Of hierdie podcast eksplisiete inhoud bevat 