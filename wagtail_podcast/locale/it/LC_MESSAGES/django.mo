��    ,      |  ;   �      �     �     �     �     �               !     *  $   J  	   o     y     �      �     �     �  "   �               %     *     B     H     [     d  ,   x     �     �     �  
   �     �  	                  -  	   ?  	   I     S     \     {     �     �  5   �  -   �  B       N     W     f     �     �     �  	   �  $   �  #   �  	   	  (   	  !   >	  !   `	     �	     �	     �	     �	  	   �	     �	     �	     �	     
     
     
  5   /
     e
     �
     �
     �
     �
     �
     �
     �
          +  	   9     C     O     i     �     �  '   �  '   �               *       $   '                                 "                !      	            &            
   #                     %         )             (   +                ,                           Archive Author Author of particular podcast Author of podcast Blubrry Podcast Url Bonus Category Category of the podcast address Content that accompanies the podcast Copyright Copyright holder of podcast Cover image for podcast Description of nature of podcast Description of podcast Direct Email address of the podcast owner Episode Type Explicit Full Google Play Podcast Url Image Itunes Podcast Url Language Language of podcast Main text page that appears on the root page No description available No text available Owner email Owner name Owner of the podcast channel Page Text Podcast Podcast Directory URLs Podcast Root Page Recording Subscribe Subtitle Subtitle of particular podcast Subtitle of podacst Trailer Type of episode Whether the podcast channel contains explicit content Whether the podcast contains explicit content Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: Alessandro Bruni <alessandro.bruni@gmail.com>
Language-Team: 
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Archivio Autore/autrice Autore/autrice dell'episodio Autore/autrice del podcast Blubrry Podcast Url Bonus Categoria Categoria dell'indirizzo del podcast Testo di accompagnamento al podcast Copyright Proprietario/a del copyright del podcast Immagine di copertina del podcast Descrizione del genere di podcast Descrizione del podcast Diretto Email del titolare del podcast Tipo dell'episodio Esplicito Intero Google Play Podcast Url Foto Itunes Podcast Url Lingua Lingua del podcast Testo di presentazione che appare sulla pagina radice Nessuna descrizione disponibile Nessun testo disponibile Email del titolare Nome del titolare Titolare del canale podcast Testo della pagina Podcast Podcast Directory URLs Pagina Radice del Podcast Registrazione Subscribe Sottotitolo Sottotitolo dell'episodio Sottotitolo del podcast Trailer Tipo di episodio Il podcast contiene contenuto esplicito L'episodio contiene contenuto esplicito 